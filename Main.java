/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OptimalCharging;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Emma
 */
public class Main {
    public static void main(String[] arg) throws FileNotFoundException{
    
    
        //ASSUMED VALUES
        int nPeriods = 168;
        int nVehicles = 1;
        int numberOfStates = 150;
        double[] gamma = new double[nVehicles];
        Arrays.fill(gamma, 0.16666666666667);
        double[] chargingMax = new double[nVehicles];
        Arrays.fill(chargingMax, 20); //Only testing, normally 20
        double[] dischargingMax = new double[nVehicles]; //Only testing, normally 20
        Arrays.fill(dischargingMax, 20);
        double[] pi = new double[nPeriods];
        Arrays.fill(pi, 10);
        double alpha = 0.4;
        double[] etaCharging = new double[nVehicles];
        Arrays.fill(etaCharging, 0.9);
        double[] etaDischarging = new double[nVehicles];
        Arrays.fill(etaDischarging, 1); //assume for simplicity that there is no loss when discharging
        double[] rho = new double[nVehicles];
        Arrays.fill(rho, 23.4); 
        double[] stateMax = new double[nVehicles];
        Arrays.fill(stateMax,60);
        double[] stateMin = new double[nVehicles];
        for(int v=1 ; v <= nVehicles ; v++){
            stateMin[v-1] = 0.2*stateMax[v-1];
        }
        //double[] p = new double[nPeriods];
        //Arrays.fill(p, 1); 
        double[] engineMax = new double[nVehicles];
        Arrays.fill(engineMax, 30);
        double beta=0.00018; //0 for én bil 0.18 for 
              
        //DRIVING DATA

        double d[][] = new double[nVehicles][nPeriods];
        
        if(nVehicles == 1){
            //I read the file "drivingDataOneCar.txt" and create a scanner for it
            File drivingData = new File("drivingDataOneCar.txt");
            Scanner s1 = new Scanner(drivingData);
        
            //For one car        
            for(int v=1 ; v <= nVehicles ; v++){
                for(int t=1 ; t<= nPeriods ; t++){
                    d[v-1][t-1] = s1.nextDouble();
                    //System.out.println(d[v-1][t-1]);
                }
            } 
        }
        
        if(nVehicles == 2){
            File drivingData = new File("drivingDataTwoCars.txt");
            Scanner s4 = new Scanner(drivingData);
            
            for(int v=1 ; v <= nVehicles ; v++){
                //System.out.println("demand for vehicle "+v);
                for(int t=1 ; t<= nPeriods ; t++){
                    d[v-1][t-1] = s4.nextDouble();
                    //System.out.println(d[v-1][t-1]);
                }
            }
            
        }
        
        if(nVehicles == 5){
            File drivingData = new File("drivingDataFiveCars.txt");
            Scanner s3 = new Scanner(drivingData);
            
            //Assign data for the five cars
            for(int v=1 ; v <= nVehicles ; v++){
                for(int t=1 ; t <= nPeriods ; t++){
                    d[v-1][t-1] = s3.nextDouble();
                    //System.out.println("d_v"+v+"_t"+(t-1)+" = "+d[v-1][t-1]);
                }
            }            
        }
        
        if(nVehicles == 10){
            File drivingData = new File("drivingDataTenCars.txt");
            Scanner s5 = new Scanner(drivingData);
            
            for(int v=1 ; v <= nVehicles ; v++){
                for(int t=1 ; t <= nPeriods ; t++){
                    d[v-1][t-1] = s5.nextDouble();
                    //System.out.println("d_v"+v+"_t"+(t-1)+" = "+d[v-1][t-1]);
                }
            }
        }
        
                
        double delta[][] = new double[nVehicles][nPeriods];
        for(int v=1 ; v <= nVehicles ; v++){
            for(int t=1; t <= nPeriods ; t++){
                if(d[v-1][t-1]>0){
                    delta[v-1][t-1] = 1;
                }else{
                    delta[v-1][t-1] = 0;
                }
            }
        }
        
        //Now spot prices
        //File highVolPrices = new File("highVolPrices.txt");
        File prices = new File("highestVolPrices.txt");
        Scanner s2 = new Scanner(prices);
        
        double[] p = new double[nPeriods];
        for(int t=1 ; t <= nPeriods ; t++){
            p[t-1] = s2.nextDouble();
            //System.out.println("p_"+t+" = "+p[t-1]);
        }
        
        //Create an instance of the problem
        OptimalChargingProblem ocp = new OptimalChargingProblem(nPeriods, nVehicles, p, pi, d, delta, etaCharging, etaDischarging, rho, gamma, alpha, stateMin, stateMax, chargingMax,
        dischargingMax, engineMax, beta);
        
        //Set the start and ending state
        double[] l_T = new double[nVehicles];
        double[] l_0 = new double[nVehicles];
        for(int v=1 ; v <= ocp.getnVehicles() ; v++){
            l_T[v-1] = stateMax[v-1];
            l_0[v-1] = l_T[v-1];
        }
                        
        //Discretizise the battery state for each vehicle
        double[] length = new double[ocp.getnVehicles()];
        for(int v=1 ; v <= ocp.getnVehicles() ; v++){
            length[v-1] = (stateMax[v-1] - stateMin[v-1])/(numberOfStates - 1);
        }

        double states[][] = new double[ocp.getnVehicles()][numberOfStates];
        for(int v=1 ; v <= ocp.getnVehicles() ; v++){
            for(int i=1 ; i <= numberOfStates ; i++){
                if(i > 1){
                    states[v-1][i-1] = stateMin[v-1] + (i-1)*length[v-1];
                
                }else{
                    states[v-1][i-1] = stateMin[v-1];
                }        
            }
        }
        
        //Creating array that holds the numbers from 1 to the number of states
        int[] permuteArray = new int[numberOfStates];
        for(int s=1 ; s <= numberOfStates ; s++){
            permuteArray[s-1] = s;                       
        }
        
        int[][] vectors = Permutations.permute(permuteArray, ocp.getnVehicles());
        //I use these vectors to look at all possible combinations of states for the vehicles, given the states I have calculated above
        //This means that I will use "vectors" to know which index I put in to "states"-vector.
        int numberOfVectors = vectors[0].length;
        /*
        for(int i=1; i<=numberOfVectors ;i++){
            for(int v=1 ; v <= ocp.getnVehicles() ; v++){
                System.out.println("v"+i+"["+v+"] = "+vectors[v-1][i-1]);
            }
        }
        */   
              
        //DEFINING OBJECTS
        
        //The cost of going to state s (only saved for one period
        double J[] = new double[numberOfVectors];
        
        //Stores values that are used to calculate the cost of each vehicle going from one state to another
        double u_plus[][][] = new double[ocp.getnVehicles()][numberOfVectors][numberOfVectors];
        double u_minus[][][] = new double[ocp.getnVehicles()][numberOfVectors][numberOfVectors];
        double w[][][] = new double[ocp.getnVehicles()][numberOfVectors][numberOfVectors];
        double q[] = new double[ocp.getnPeriods()];
        
        double K[][][] = new double[ocp.getnVehicles()][numberOfVectors][numberOfVectors]; //Stores the cost of each vehicle in each combination of states
        double H[][] = new double[numberOfVectors][numberOfVectors]; //Stores the total cost for all vehicles in each combination of states       
        
        //Stores optimal charging, discharging and engine use when vehicle v is at state s in period t
        double u_plus_star[][][] = new double[ocp.getnVehicles()][numberOfVectors][ocp.getnPeriods()];
        double u_minus_star[][][] = new double[ocp.getnVehicles()][numberOfVectors][ocp.getnPeriods()]; 
        double w_star[][][] = new double[ocp.getnVehicles()][numberOfVectors][ocp.getnPeriods()];
        
        
        //DYNAMIC PROGRAMMING ALGORITHM
                
        for(int t=ocp.getnPointsInTime() ; t >= 1 ; t--){ //Start at the last point in time and move backwards.
                           
            if(t < ocp.getnPointsInTime()){ //The last, second to last and first point in time gets special treatment.
                
                if(t< ocp.getnPointsInTime() -1){
                    
                    if(t < 2){
                        
                            //At time 0 the state is fixed to l_0 for each vehicle, so I do not loop over s.
                            for(int c=1 ; c <= numberOfVectors ; c++){ //I loop over all the possible states at time 1.
                                
                                q[t-1] = 0;
                                
                                for(int v=1 ; v <= ocp.getnVehicles() ; v++){
                                    
                                    //Calculation of charging, discharging and engine use.
                                    u_plus[v-1][0][c-1] = Math.max((1-ocp.getDelta()[v-1][t-1])*(states[v-1][vectors[v-1][c-1]-1] - l_0[v-1])*Math.pow(ocp.getEtaCharging()[v-1], -1), 0);
                                    u_minus[v-1][0][c-1] = Math.max((1-ocp.getDelta()[v-1][t-1])*(l_0[v-1] - states[v-1][vectors[v-1][c-1]-1]), 0);
                                    q[t-1] = q[t-1] + u_plus[v-1][0][c-1] - u_minus[v-1][0][c-1]; 
                                    w[v-1][0][c-1] = ocp.getDelta()[v-1][t-1]*(Math.pow(ocp.getGamma()[v-1],-1)*(states[v-1][vectors[v-1][c-1]-1] - l_0[v-1]) 
                                            + d[v-1][t-1])*Math.pow(ocp.getRho()[v-1], -1);                                    
                                }
                                
                                for(int v=1 ; v <= ocp.getnVehicles() ; v++){
                                    
                                    //Calculationg the cost of vehicle v going to state c at time 1.
                                    if(u_plus[v-1][0][c-1] <= ocp.getChargingMax()[v-1] && u_minus[v-1][0][c-1] <= ocp.getDischargingMax()[v-1] 
                                            && 0 <= w[v-1][0][c-1] && w[v-1][0][c-1] <= ocp.getEngineMax()[v-1]){
                                    
                                        K[v-1][0][c-1] = (p[t-1] + ocp.getBeta()*q[t-1])*(u_plus[v-1][0][c-1] - ocp.getEtaDischarging()[v-1]*u_minus[v-1][0][c-1]) + pi[t-1]*w[v-1][0][c-1] 
                                                + alpha*u_minus[v-1][0][c-1];
                                    
                                    }else{ //If the limits for charging, discharging and enginge use is not met, the cost is set to infinity,
                                           //so that it will never be the minimum over all the K's.
                                    
                                        K[v-1][0][c-1] = Double.POSITIVE_INFINITY;
                                    }
                                }
                                
                                
                            }                          
                            
                            //Calculating the total cost of going to state vector c in time 2
                            for(int c=1 ; c <= numberOfVectors ; c++){
                                
                                H[0][c-1] = J[c-1];
                                
                                for(int v=1 ; v <= ocp.getnVehicles() ; v++){
                                    
                                    H[0][c-1] = H[0][c-1]+ K[v-1][0][c-1];                                     
                                }                                
                            }
                            
                            //Calculating J as the minimum over all the total costs and setting u+,u- and w star to have the value of the state that minimizes J
                            J[0] = H[0][0];                            
                            for(int v=1 ; v <= ocp.getnVehicles() ; v++){
                                
                                u_plus_star[v-1][0][t-1] = u_plus[v-1][0][0];
                                u_minus_star[v-1][0][t-1] = u_minus[v-1][0][0];
                                w_star[v-1][0][t-1] = w[v-1][0][0];
                            }   
                            
                            for(int c=2 ; c <= numberOfVectors ; c++){
                                
                                if(H[0][c-1] < J[0]){
                                    
                                    J[0] = H[0][c-1];
                                    
                                    for(int v=1 ; v<= ocp.getnVehicles() ; v++){
                                        
                                        u_plus_star[v-1][0][t-1] = u_plus[v-1][0][c-1];
                                        u_minus_star[v-1][0][t-1] = u_minus[v-1][0][c-1];
                                        w_star[v-1][0][t-1] = w[v-1][0][c-1];   
                                        
                                    }
                                }                                
                            }
                            
                            //Print the total optimal cost (optimal objective function value)                            
                            System.out.println("Total cost for vehicles: J_"+t+"="+J[0]);
                                                                   
                    }else{ //from time 1 to T-2 I look at all combinations of state vectors, so I loop over both s and c
                        System.out.println("t = "+(t-1));
                        for(int s=1 ; s <= numberOfVectors ; s++){
                            
                            for(int c=1 ; c <= numberOfVectors ; c++){
                                
                                q[t-1] = 0;
                                
                                for(int v=1 ; v <= ocp.getnVehicles() ; v++){
                                
                                    //Calculate the charging, discharging and use of engine for each combination of states.
                                    u_plus[v-1][s-1][c-1] = Math.max((1-ocp.getDelta()[v-1][t-1])*(states[v-1][vectors[v-1][c-1]-1] - states[v-1][vectors[v-1][s-1]-1])
                                            *Math.pow(ocp.getEtaCharging()[v-1],-1), 0);
                                    u_minus[v-1][s-1][c-1] = Math.max((1-ocp.getDelta()[v-1][t-1])*(states[v-1][vectors[v-1][s-1]-1] - states[v-1][vectors[v-1][c-1]-1]), 0);
                                    q[t-1] = q[t-1] + u_plus[v-1][s-1][c-1] - u_minus[v-1][s-1][c-1];
                                    w[v-1][s-1][c-1] = ocp.getDelta()[v-1][t-1]*(Math.pow(ocp.getGamma()[v-1],-1)*(states[v-1][vectors[v-1][c-1]-1] - states[v-1][vectors[v-1][s-1]-1]) 
                                            + d[v-1][t-1])*Math.pow(ocp.getRho()[v-1],-1);
                                }
                                
                                for(int v=1 ; v <= ocp.getnVehicles() ; v++){
                                    
                                    //To rule out combination af states that is not feasible according to the capacities, I set the cost to infinity.
                                    if(u_plus[v-1][s-1][c-1] <= ocp.getChargingMax()[v-1] && u_minus[v-1][s-1][c-1] <= ocp.getDischargingMax()[v-1] 
                                            && 0 <= w[v-1][s-1][c-1] && w[v-1][s-1][c-1] <= ocp.getEngineMax()[v-1]){
                                        
                                        K[v-1][s-1][c-1] = (p[t-1] + ocp.getBeta()*q[t-1])*(u_plus[v-1][s-1][c-1] - ocp.getEtaDischarging()[v-1]*u_minus[v-1][s-1][c-1]) 
                                                + pi[t-1]*w[v-1][s-1][c-1] + alpha*u_minus[v-1][s-1][c-1];                                            
                                    }else{
                                        
                                        K[v-1][s-1][c-1] = Double.POSITIVE_INFINITY;
                                    }
                                    //System.out.println("K_"+v+"_"+s+"_"+c+" = "+K[v-1][s-1][c-1]);
                                }
                            }
                        }
                        
                        //Calculating the total cost of going from states in vector s til states in vector c
                        for(int s=1 ; s <= numberOfVectors; s++){
                            for(int c=1 ; c <= numberOfVectors ; c++){
                                H[s-1][c-1] = J[c-1];
                                for(int v=1 ; v <= ocp.getnVehicles() ; v++){
                                    H[s-1][c-1] = H[s-1][c-1] + K[v-1][s-1][c-1];
                                }
                                //System.out.println("H_"+s+"_"+c+" = "+H[s-1][c-1]);
                            }                        
                        }
                        
                        //Finding the minimal cost for each vector
                        for(int s=1 ; s <= numberOfVectors ; s++){
                            J[s-1] = H[s-1][0];
                            for(int v=1 ; v <= ocp.getnVehicles() ; v++){                                
                                u_plus_star[v-1][s-1][t-1] = u_plus[v-1][s-1][0];
                                u_minus_star[v-1][s-1][t-1] = u_minus[v-1][s-1][0];
                                w_star[v-1][s-1][t-1] = w[v-1][s-1][0];
                                
                            }  
                            
                            for(int c=1 ; c <= numberOfVectors ; c++){
                                if(H[s-1][c-1] < J[s-1]){
                                    J[s-1] = H[s-1][c-1];
                                    for(int v=1 ; v<= ocp.getnVehicles() ; v++){
                                        u_plus_star[v-1][s-1][t-1] = u_plus[v-1][s-1][c-1];
                                        u_minus_star[v-1][s-1][t-1] = u_minus[v-1][s-1][c-1];
                                        w_star[v-1][s-1][t-1] = w[v-1][s-1][c-1];
                                        
                                    }
                                }
                            }
                            //System.out.println("J_"+t+"("+s+")="+J[s-1]);
                        }
                        
                        
                    }
                }else{ //At time T-1 we do not need to minimize over battery states in time T, since it has a known value.
                    
                    for(int s=1 ; s <= numberOfVectors ; s++){
                        
                        //I do not loop over c, since there is only one possible state for each vehicle at time T, l_T.                        
                        q[t-1] = 0;
                        
                        for(int v=1 ; v <= ocp.getnVehicles() ; v++){
                            
                        //Calculating charging, discharging and engine use.
                            u_plus[v-1][s-1][0] = Math.max((1-ocp.getDelta()[v-1][t-1])*(l_T[v-1] - states[v-1][vectors[v-1][s-1]-1])*Math.pow(ocp.getEtaCharging()[v-1], -1), 0);
                            u_minus[v-1][s-1][0] = Math.max((1-ocp.getDelta()[v-1][t-1])*(states[v-1][vectors[v-1][s-1]-1]-l_T[v-1]), 0);
                            q[t-1] = q[t-1] + u_plus[v-1][s-1][0] - u_minus[v-1][s-1][0];
                            w[v-1][s-1][0] = ocp.getDelta()[v-1][t-1]*(Math.pow(ocp.getGamma()[v-1],-1)*(l_T[v-1]-states[v-1][vectors[v-1][s-1]-1])+d[v-1][t-1])
                                    *Math.pow(ocp.getRho()[v-1],-1);                            
                        }
                        
                        for(int v=1 ; v <= ocp.getnVehicles() ; v++){
                        
                            if(u_plus[v-1][s-1][0] <= ocp.getChargingMax()[v-1] && u_minus[v-1][s-1][0] <= ocp.getDischargingMax()[v-1] 
                                        && 0 <= w[v-1][s-1][0] && w[v-1][s-1][0] <= ocp.getEngineMax()[v-1]){                               
                            K[v-1][s-1][0] = (p[t-1] + ocp.getBeta()*q[t-1])*(u_plus[v-1][s-1][0]-ocp.getEtaDischarging()[v-1]*u_minus[v-1][s-1][0]) 
                                    + pi[t-1]*w[v-1][s-1][0] + alpha*u_minus[v-1][s-1][0];
                            }else{
                                K[v-1][s-1][0] = Double.POSITIVE_INFINITY;
                            }
                            //System.out.println("K_"+v+"_"+s+"_0 = "+K[v-1][s-1][0]);
                        }
                    }
                    
                                        
                    //Set J to be the total cost
                    for(int s=1 ; s <= numberOfVectors ; s++){
                        J[s-1] = J[0];
                        for(int v=1 ; v <= ocp.getnVehicles() ; v++){
                            J[s-1] = J[s-1] + K[v-1][s-1][0];                            
                            u_plus_star[v-1][s-1][t-1] = u_plus[v-1][s-1][0];
                            u_minus_star[v-1][s-1][t-1] = u_minus[v-1][s-1][0];
                            w_star[v-1][s-1][t-1] = w[v-1][s-1][0];
                            
                        }
                        //System.out.println("J_"+t+"("+s+")="+J[s-1]);
                    }
                }
                
            }else{ //At the time T the cost is 0.    
                for(int v=1 ; v <= ocp.getnVehicles() ; v++){
                    J[0]=0; 
                }    
            }       
        }
        
        

    
        //FINDING THE SOLUTION
        
        double[][] l_solution = new double[ocp.getnVehicles()][ocp.getnPointsInTime()];
        double[][] u_plus_solution = new double[ocp.getnVehicles()][ocp.getnPeriods()];
        double[][] u_minus_solution = new double[ocp.getnVehicles()][ocp.getnPeriods()];
        double[][] w_solution = new double[ocp.getnVehicles()][ocp.getnPeriods()];
        int[] stateIndex = new int[ocp.getnVehicles()];
        int stateVector = 0;
        int[] array = new int[ocp.getnVehicles()];
        Arrays.fill(stateIndex, 0);

               
        for(int t=1 ; t <= ocp.getnPeriods() ; t++){ //Now I start at time 0 and move forwards to find the solution
            
            if( t < 2 ){ //At time 0 I know the state of the battery
                
                for(int v=1 ; v <= ocp.getnVehicles() ; v++){
                    
                    l_solution[v-1][t-1] = l_0[v-1]; 
                                       
                    //I already know the optimal charging, dicharging and enginge use in the first period
                    u_plus_solution[v-1][t-1] = u_plus_star[v-1][0][t-1];
                    u_minus_solution[v-1][t-1] = u_minus_star[v-1][0][t-1];
                    w_solution[v-1][t-1] = w_star[v-1][0][t-1];
                    
                    //From that I can calculate the optimal state at time 1
                    l_solution[v-1][t] = l_solution[v-1][t-1] + (1-ocp.getDelta()[v-1][t-1])*(ocp.getEtaCharging()[v-1]*u_plus_solution[v-1][t-1] - u_minus_solution[v-1][t-1]) 
                            + ocp.getDelta()[v-1][t-1]*ocp.getGamma()[v-1]*(ocp.getRho()[v-1]*w_solution[v-1][t-1] - ocp.getD()[v-1][t-1]);
                                                           
                    //But which index of the states does the value l_solution[v-1] correspond to?
                    for(int s=1 ; s <= numberOfStates ; s++){
                        if( l_solution[v-1][t] <= states[v-1][s-1] + .1 && l_solution[v-1][t] >= states[v-1][s-1] - .1){ 
                        //I give the value of the state a buffer of .1 to be sure that I can reconice the state even though they do not have exacly the same decimals
                            stateIndex[v-1] = s;
                        }
                    }
                                                            
                }
                
                //Finding the vector that the states corresponds to
                for(int s=1 ; s <= numberOfVectors ; s++){
                    for(int v=1 ; v <= ocp.getnVehicles() ; v++){
                        array[v-1] = vectors[v-1][s-1];                        
                    }                     
                    if( Arrays.equals(stateIndex, array) ) {
                        stateVector = s;
                    }       
                }                                
                
            }else{
                
                if( t < ocp.getnPeriods() ){
                    
                    for(int v=1 ; v <= ocp.getnVehicles() ; v++){
                                                
                        //Here I am at any state between 1 and T-1
                        //I know the optimal state at this time, so I can calculate the optimal charging, discharging and battery use in period [t,t+1]
                        u_plus_solution[v-1][t-1] = u_plus_star[v-1][stateVector-1][t-1];
                        u_minus_solution[v-1][t-1] = u_minus_star[v-1][stateVector-1][t-1];
                        w_solution[v-1][t-1] = w_star[v-1][stateVector-1][t-1];
                                               
                        //I can now calculate the optimal state at the next point in time
                        l_solution[v-1][t] = l_solution[v-1][t-1] + (1-ocp.getDelta()[v-1][t-1])*(ocp.getEtaCharging()[v-1]*u_plus_solution[v-1][t-1] - u_minus_solution[v-1][t-1]) 
                                + ocp.getDelta()[v-1][t-1]*ocp.getGamma()[v-1]*(ocp.getRho()[v-1]*w_solution[v-1][t-1] - ocp.getD()[v-1][t-1]);
                        
                        
                        //Find index that the state corresponds to
                        for(int s=1 ; s <= numberOfStates ; s++){
                            if( l_solution[v-1][t] <= states[v-1][s-1] + .1 && l_solution[v-1][t] >= states[v-1][s-1] - .1){
                                stateIndex[v-1] = s;
                            }
                        }
                    }
                    
                    //Finding the vector that the states corresponds to
                    for(int s=1 ; s <= numberOfVectors ; s++){
                        for(int v=1 ; v <= ocp.getnVehicles() ; v++){
                        array[v-1] = vectors[v-1][s-1];
                        }                    
                        if( Arrays.equals(stateIndex, array) )
                        stateVector = s;    
                    }
                        
                }else{ //at time T-1 and T
                    //This point in time is only different because I will not calculate the optimal state in the next period or its index, as it is known to be l_T                   
                    
                    for(int v=1 ; v <= ocp.getnVehicles() ; v++){
                        
                        //I know the optimal state at time T-1, so I find optimal charging, discharging and engine use in the last period from T-1 to T.
                        u_plus_solution[v-1][t-1] = u_plus_star[v-1][stateVector-1][t-1];
                        u_minus_solution[v-1][t-1] = u_minus_star[v-1][stateVector-1][t-1];
                        w_solution[v-1][t-1] = w_star[v-1][stateVector-1][t-1]; 
                                           
                        //The battery state at time T is l_T
                        l_solution[v-1][t] = l_T[v-1];
                                               
                    }
                }            
            }
        }        
        
        
        
        
        //Printing the solution
        for(int v=1 ; v <= ocp.getnVehicles() ; v++){
            System.out.println("vehicle"+v);
            System.out.println("Battery state");
            for(int t=1 ; t <= ocp.getnPointsInTime() ; t++){
                System.out.println(+l_solution[v-1][t-1]);                
            }
            System.out.println("Charging");
            for(int t=1 ; t <= ocp.getnPeriods() ; t++){
                System.out.println(+u_plus_solution[v-1][t-1]);                
            }
            System.out.println("Discharging");
            for(int t=1 ; t <= ocp.getnPeriods() ; t++){
                System.out.println(+u_minus_solution[v-1][t-1]);
                
            }
            System.out.println("Engine use");
            for(int t=1 ; t <= ocp.getnPeriods() ; t++){
                System.out.println(+w_solution[v-1][t-1]);                
            }
        }
        
        
                /*
                System.out.println("l_"+(t-1)+" = "+l_solution[v-1][t-1]);
                System.out.println("u_plus_"+(t-1)+" = "+u_plus_solution[v-1][t-1]);
                System.out.println("u_minus_"+(t-1)+" = "+u_minus_solution[v-1][t-1]);
                System.out.println("w_"+(t-1)+" = "+w_solution[v-1][t-1]);
                */
    }    
}

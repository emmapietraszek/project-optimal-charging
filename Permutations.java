/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OptimalCharging;

/**
 *
 * @author Emma
 */
public class Permutations {
    
    //Method that generates all the possible state vectors
    static int[][] permute(int[] a, int k) {
        int n = a.length;

        int[] indexes = new int[k];
        int total = (int) Math.pow(n, k);
        int[][] permutations= new int[k][total];
        int[] snapshot = new int[k];
        while (total-- > 0) {
            //System.out.println("total "+total);
            for (int i = 0; i < k; i++){
                snapshot[i] = a[indexes[i]];
                permutations[i][total] = snapshot[i];
                
                //System.out.print(permutations[i][total]);
            }
            
            //System.out.println("");
            for (int i = 0; i < k; i++) {
                if (indexes[i] >= n - 1) {
                    indexes[i] = 0;
                } else {
                    indexes[i]++;
                    break;
                }
            }
        }
        return permutations;
    }
    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OptimalCharging;

/**
 *
 * @author Emma
 */
public class OptimalChargingProblem {

    //This class contains a constructor of the optimal chargning problem and getters for the parameters.
    
    //DEFINE the  number of time periods and vehicles.
    private final int nPeriods;
    private final int nVehicles;
    
    //DATA
    private final double p[]; //The price of electricity with no effect form the fleet
    private final double pi[]; //The litre price of diesel
    private final double d[][]; //Driving demand for the vehicle
    private final double delta[][]; //Whether or not the vehicle os driving
    
    //ASSUMED PARAMETERS
    private final double etaCharging[]; //Efficientcy of charging
    private final double etaDischarging[]; //Efficientcy of discharging
    private final double rho[]; //Efficientcy of the engine
    private final double gamma[]; // Efficientcy of the vehicle (kWh per km)
    private final double alpha; //Scalar that determins the battery wear
    private final double stateMin[]; //Minimum state of charge
    private final double stateMax[]; //Maximum state of charge
    private final double chargingMax[]; //Limit of charging in one period
    private final double dischargingMax[]; //Limit of discharging in one period
    private final double engineMax[]; //Maximum use of the engine
    private final double beta; //The effect of the fleets electricity consumption on the price of electricity
    
    //CONSTRUCTOR that creates an instance of the optimal charging problem
    public OptimalChargingProblem(int nPeriods,int nVehicles,double p[],double pi[],double d[][],double delta[][],double etaCharging[],
        double etaDischarging[],double rho[],double gamma[],double alpha,double stateMin[],double stateMax[],double chargingMax[],
        double dischargingMax[],double engineMax[],double beta){
        this.nPeriods = nPeriods;
        this.nVehicles = nVehicles;
        this.p = p;
        this.pi = pi;
        this.d = d;
        this.delta = delta;
        this.etaCharging = etaCharging;
        this.etaDischarging = etaDischarging;
        this.rho = rho;
        this.gamma = gamma;
        this.alpha = alpha;
        this.stateMin = stateMin;
        this.stateMax = stateMax;
        this.chargingMax = chargingMax;
        this.dischargingMax = dischargingMax;
        this.engineMax = engineMax;
        this.beta = beta;
    
    }
    
    //GETTERS FOR ALL THE PARAMETERS

    public int getnPeriods() {
        return nPeriods;
    }

    public int getnPointsInTime(){
        int nPointsInTime = nPeriods + 1;
        return nPointsInTime;
    }
    
    public int getnVehicles() {
        return nVehicles;
    }

    public double[] getP() {
        return p;
    }

    public double[] getPi() {
        return pi;
    }

    public double[][] getD() {
        return d;
    }

    public double[][] getDelta() {
        return delta;
    }

    public double[] getEtaCharging() {
        return etaCharging;
    }

    public double[] getEtaDischarging() {
        return etaDischarging;
    }

    public double[] getRho() {
        return rho;
    }

    public double[] getGamma() {
        return gamma;
    }

    public double getAlpha() {
        return alpha;
    }

    public double[] getStateMin() {
        return stateMin;
    }

    public double[] getStateMax() {
        return stateMax;
    }

    public double[] getChargingMax() {
        return chargingMax;
    }

    public double[] getDischargingMax() {
        return dischargingMax;
    }

    public double[] getEngineMax() {
        return engineMax;
    }

    public double getBeta() {
        return beta;
    }
    


    
}
